# lp-theme

## Instalace

Nejprve je nutné si naklonovat do své lokální složky repozitář: `git clone git@gitlab.com:kuba.drbohlav/lp-theme.git .`

Na stroji musí být nainstalován `nodejs` a `npm`.

**Instalace potřebných balíčků**
```
npm install
```

**Zbuildění produkční verze**
```
npm run build
```
Vytvoří se složka `dist/`. V ní je soubor `index.html` a složka `assets/`, která obsahuje všechny styly, skripty, fonty a obrázky. 